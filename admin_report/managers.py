from postingan.models import Postingan, Tanggapan
from .models import ReportCause, ReportedPostingan, ReportedTanggapan, TakenDownPostingan, TakenDownTanggapan
from authentication.models import Mahasiswa, Pengguna, Admin
from django.db import transaction
from django.shortcuts import get_object_or_404

class ReportedManager:
    
    @staticmethod
    def create_reported_postingan(postingan_id: str, reporter: Pengguna, cause: ReportCause, cause_elaboration: str):
        with transaction.atomic():
            postingan = get_object_or_404(Postingan, id=postingan_id)
            reporter = get_object_or_404(Mahasiswa, pengguna=reporter)
            if ReportedPostingan.objects.filter(postingan=postingan, reporter=reporter).exists():
                raise Exception("Postingan ini telah kamu report.")

            reported_postingan = ReportedPostingan(postingan = postingan,
                                                reporter = reporter,
                                                cause = cause,
                                                cause_elaboration = cause_elaboration)
            

            reported_postingan.save()
            return reported_postingan
        

    @staticmethod
    def get_all_reported_postingan():  
       return ReportedPostingan.objects.all()

    @staticmethod
    def get_reported_postingan(postingan_id: str):  
       return get_object_or_404(ReportedPostingan, id=postingan_id)
    

    @staticmethod
    def create_reported_tanggapan(tanggapan_id: str, reporter: Pengguna, cause: ReportCause, cause_elaboration: str):
        with transaction.atomic():
            tanggapan = get_object_or_404(Tanggapan, id=tanggapan_id)
            reporter = get_object_or_404(Mahasiswa, pengguna=reporter)
            if ReportedTanggapan.objects.filter(tanggapan=tanggapan, reporter=reporter).exists():
                raise Exception("Tanggapan ini telah kamu report.")
            reported_tanggapan = ReportedTanggapan(tanggapan = tanggapan,
                                                reporter = reporter,
                                                cause = cause,
                                                cause_elaboration = cause_elaboration)
            
            reported_tanggapan.save()
            return reported_tanggapan
        

    @staticmethod
    def get_all_reported_tanggapan():  
       return ReportedTanggapan.objects.all()

    @staticmethod
    def get_reported_tanggapan(tanggapan_id: str):  
       return get_object_or_404(ReportedTanggapan, id=tanggapan_id)


class TakeDownManager:

    @staticmethod
    def take_down_postingan(postingan_id, pengguna: Pengguna):
        with transaction.atomic():
            postingan = get_object_or_404(Postingan, id=postingan_id)
            
            if postingan.is_taken_down:
                raise Exception("Postingan ini telah ditakedown.")
        
            postingan.is_taken_down = True
            postingan.save()

            admin = get_object_or_404(Admin, pengguna=pengguna)
            taken_down_postingan = TakenDownPostingan.objects.create(
                postingan=postingan, 
                admin=admin
            )
            return taken_down_postingan

    @staticmethod
    def take_down_tanggapan(tanggapan_id, pengguna: Pengguna):
        with transaction.atomic():
            tanggapan = get_object_or_404(Tanggapan, id=tanggapan_id)
            if tanggapan.is_taken_down:
                raise Exception("Tanggapan ini telah ditakedown.")
            
            tanggapan.is_taken_down = True
            tanggapan.save()
            
            admin = get_object_or_404(Admin, pengguna=pengguna)
            taken_down_tanggapan = TakenDownTanggapan.objects.create(
                tanggapan=tanggapan, 
                admin=admin
            )
            return taken_down_tanggapan
        
        
    @staticmethod
    def restore_postingan(postingan_id):
        with transaction.atomic():
            postingan = get_object_or_404(Postingan, id=postingan_id)
            
            if not postingan.is_taken_down:
                raise Exception("Postingan ini telah direstore.")

            postingan.is_taken_down = False
            postingan.save()
            
            TakenDownPostingan.objects.filter(postingan=postingan).delete()


    @staticmethod
    def restore_tanggapan(tanggapan_id):
        with transaction.atomic():
            tanggapan = get_object_or_404(Tanggapan, id=tanggapan_id)

                        
            if not tanggapan.is_taken_down:
                raise Exception("Tanggapan ini telah direstore.")
            
            tanggapan.is_taken_down = False
            tanggapan.save()
            
            TakenDownTanggapan.objects.filter(tanggapan=tanggapan).delete()

