from django.shortcuts import render
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from .managers import ReportedManager, TakeDownManager
from postingan.models import Postingan
from authentication.models import Mahasiswa
from rest_framework import status
from django.http import Http404
from .serializers import ReportPostinganSerializer, ReportedPostinganSerializer, ReportTanggapanSerializer, ReportedTanggapanSerializer
from .serializers import TakenDownPostinganSerializer, TakenDownTanggapanSerializer
from rest_framework.permissions import IsAuthenticated
from authentication.permissions import IsAdmin, IsMahasiswa
from django.shortcuts import get_object_or_404
from .models import TakenDownPostingan, TakenDownTanggapan

@api_view(['POST'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def report_postingan_view(request, postingan_id):
    serializer = ReportPostinganSerializer(data=request.data)
    
    
    if serializer.is_valid():
        try:
            reported_postingan = ReportedManager.create_reported_postingan(
                reporter=request.user,
                postingan_id=postingan_id,
                cause=serializer.validated_data['cause'],
                cause_elaboration=serializer.validated_data['cause_elaboration']
            )
            return Response({'message': 'Report berhasil disubmit.', 
                             'reported_postingan': ReportPostinganSerializer(reported_postingan).data},
                            status=status.HTTP_201_CREATED)
        
        except Exception as e:
            return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_all_reported_postingan_view(request):
    all_reported_postingan = ReportedManager.get_all_reported_postingan()
    serializer = ReportedPostinganSerializer(all_reported_postingan, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_specific_reported_postingan_view(request, postingan_id):
    try:
        reported_postingan = ReportedManager.get_reported_postingan(postingan_id)
        serializer = ReportedPostinganSerializer(reported_postingan)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Http404:
        return Response({'error_message': 'Reported Postingan tidak ditemukan.'}, status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def report_tanggapan_view(request, tanggapan_id):
    serializer = ReportTanggapanSerializer(data=request.data)
    
    if serializer.is_valid():
        try:
            reported_tanggapan = ReportedManager.create_reported_tanggapan(
                reporter=request.user,
                tanggapan_id=tanggapan_id,
                cause=serializer.validated_data['cause'],
                cause_elaboration=serializer.validated_data['cause_elaboration']
            )
            return Response({'message': 'Report berhasil disubmit.', 
                             'reported_tanggapan': ReportTanggapanSerializer(reported_tanggapan).data},
                            status=status.HTTP_201_CREATED)
        
        except Exception as e:
            return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_all_reported_tanggapan_view(request):
    all_reported_tanggapan = ReportedManager.get_all_reported_tanggapan()
    serializer = ReportedTanggapanSerializer(all_reported_tanggapan, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_specific_reported_tanggapan_view(request, tanggapan_id):
    try:
        reported_tanggapan = ReportedManager.get_reported_tanggapan(tanggapan_id)
        serializer = ReportedTanggapanSerializer(reported_tanggapan)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Http404:
        return Response({'error_message': 'Reported Tanggapan tidak ditemukan.'}, status=status.HTTP_404_NOT_FOUND)
    
@api_view(['POST'])
@permission_classes([IsAuthenticated, IsAdmin])
def take_down_postingan_view(request, postingan_id):
    try:
        TakeDownManager.take_down_postingan(postingan_id, request.user)
        return Response({'message': 'Postingan berhasil ditakedown.'}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated, IsAdmin])
def take_down_tanggapan_view(request, tanggapan_id):
    try:
        TakeDownManager.take_down_tanggapan(tanggapan_id, request.user)
        return Response({'message': 'Tanggapan berhasil ditakedown.'}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)
    
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_all_taken_down_postingan_view(request):
    taken_down_postingans = TakenDownPostingan.objects.all()
    serializer = TakenDownPostinganSerializer(taken_down_postingans, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
@permission_classes([IsAuthenticated, IsAdmin])
def get_all_taken_down_tanggapan_view(request):
    taken_down_tanggapans = TakenDownTanggapan.objects.all()
    serializer = TakenDownTanggapanSerializer(taken_down_tanggapans, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)



@api_view(['POST'])
@permission_classes([IsAuthenticated, IsAdmin])
def restore_postingan_view(request, postingan_id):
    try:
        TakeDownManager.restore_postingan(postingan_id)
        return Response({'message': 'Postingan berhasil direstore.'}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([IsAuthenticated, IsAdmin])
def restore_tanggapan_view(request, tanggapan_id):
    try:
        TakeDownManager.restore_tanggapan(tanggapan_id)
        return Response({'message': 'Tanggapan berhasil direstore.'}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'error_message': str(e)}, status=status.HTTP_400_BAD_REQUEST)