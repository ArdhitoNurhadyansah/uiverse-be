from django.db import models
from postingan.models import Postingan, Tanggapan 
from authentication.models import Mahasiswa, Admin
import uuid

class ReportCause(models.Model):
    title = models.CharField(max_length=255)    

class Reported(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    reporter = models.ForeignKey(Mahasiswa, on_delete=models.SET_NULL, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    cause = models.ForeignKey(ReportCause, on_delete=models.CASCADE)
    cause_elaboration = models.CharField(max_length=255)
    
    class Meta:
        abstract = True
        
class ReportedPostingan(Reported):
    postingan = models.ForeignKey(Postingan, on_delete=models.CASCADE)
    
class ReportedTanggapan(Reported):
    tanggapan = models.ForeignKey(Tanggapan, on_delete=models.CASCADE)


class TakenDown(models.Model):
    admin = models.ForeignKey(Admin, on_delete=models.SET_NULL, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    class Meta:
        abstract = True
class TakenDownPostingan(TakenDown):
    postingan = models.ForeignKey(Postingan, on_delete=models.CASCADE)

class TakenDownTanggapan(TakenDown):
    tanggapan = models.ForeignKey(Tanggapan, on_delete=models.CASCADE)
