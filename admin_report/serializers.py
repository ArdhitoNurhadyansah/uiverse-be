from rest_framework import serializers
from .models import ReportedPostingan, ReportedTanggapan, TakenDownPostingan, TakenDownTanggapan
from postingan.serializers import PostinganSerializer, TanggapanSerializer

class ReportPostinganSerializer(serializers.ModelSerializer):
    cause_title = serializers.CharField(source='cause.title', read_only=True)
    class Meta:
        model = ReportedPostingan
        fields = ['cause', 'cause_title', 'cause_elaboration']

class ReportedPostinganSerializer(serializers.ModelSerializer):
    cause_title = serializers.CharField(source='cause.title', read_only=True)
    postingan = PostinganSerializer()
    class Meta:
        model = ReportedPostingan
        exclude = ['cause']
    
class ReportTanggapanSerializer(serializers.ModelSerializer):
    cause_title = serializers.CharField(source='cause.title', read_only=True)
    class Meta:
        model = ReportedTanggapan
        fields = ['cause', 'cause_title', 'cause_elaboration']

class ReportedTanggapanSerializer(serializers.ModelSerializer):
    cause_title = serializers.CharField(source='cause.title', read_only=True)
    tanggapan = TanggapanSerializer()
    class Meta:
        model = ReportedTanggapan
        exclude = ['cause']
    
class TakenDownPostinganSerializer(serializers.ModelSerializer):
    postingan = PostinganSerializer()
    class Meta:
        model = TakenDownPostingan
        fields = ['id', 'postingan', 'admin', 'timestamp']

class TakenDownTanggapanSerializer(serializers.ModelSerializer):
    tanggapan = TanggapanSerializer()
    class Meta:
        model = TakenDownTanggapan
        fields = ['id', 'tanggapan', 'admin', 'timestamp']