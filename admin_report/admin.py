from django.contrib import admin
from .models import ReportCause, ReportedPostingan, ReportedTanggapan, TakenDownPostingan, TakenDownTanggapan
# Register your models here.
admin.site.register(ReportCause)
admin.site.register(ReportedPostingan)
admin.site.register(ReportedTanggapan)
admin.site.register(TakenDownPostingan)
admin.site.register(TakenDownTanggapan)

