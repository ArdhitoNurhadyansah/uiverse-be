from django.urls import path
from .views import (report_postingan_view, get_all_reported_postingan_view, get_specific_reported_postingan_view, 
                    report_tanggapan_view, get_all_reported_tanggapan_view, get_specific_reported_tanggapan_view,
                    take_down_postingan_view, take_down_tanggapan_view, 
                    get_all_taken_down_postingan_view, get_all_taken_down_tanggapan_view,
                    restore_postingan_view, restore_tanggapan_view)

urlpatterns = [
    # Reported Postingan
    path('report-postingan/<uuid:postingan_id>/', report_postingan_view, name='report_postingan'),
    path('reported-postingan/', get_all_reported_postingan_view, name='get_all_reported_postingan'),
    path('reported-postingan/<uuid:postingan_id>/', get_specific_reported_postingan_view, name='get_reported_postingan'),

    # Reported Tanggapan
    path('report-tanggapan/<uuid:tanggapan_id>/', report_tanggapan_view, name='report_tanggapan'),
    path('reported-tanggapan/', get_all_reported_tanggapan_view, name='get_all_reported_tanggapan'),
    path('reported-tanggapan/<uuid:tanggapan_id>/', get_specific_reported_tanggapan_view, name='get_reported_tanggapan'),

    # Take Down Postingan
    path('take-down-postingan/<uuid:postingan_id>/', take_down_postingan_view, name='take_down_postingan'),
    path('take-down-tanggapan/<uuid:tanggapan_id>/', take_down_tanggapan_view, name='take_down_tanggapan'),

    # Take Down Tanggapan
    path('taken-down-postingan/', get_all_taken_down_postingan_view, name='get_all_taken_down_postingan'),
    path('taken-down-tanggapan/', get_all_taken_down_tanggapan_view, name='get_all_taken_down_tanggapan'),
    
    path('restore-postingan/<uuid:postingan_id>/', restore_postingan_view, name='restore_postingan'),
    path('restore-tanggapan/<uuid:tanggapan_id>/', restore_tanggapan_view, name='restore_tanggapan'),
]