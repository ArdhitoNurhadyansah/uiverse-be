
from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models

import json
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

class Role(models.TextChoices):
    """
    A model for the user's role.
    """
    MHS = 'mahasiswa'
    ADM = 'admin'

class Pengguna(AbstractUser):
    first_name = None
    last_name = None
    id = models.CharField(max_length=255, verbose_name="username", primary_key=True) # isi dengan username
    npm = models.CharField(max_length=255, verbose_name="npm") 
    nama = models.CharField(max_length=255)
    kd_org = models.CharField(max_length=255)
    email = models.EmailField()

    def __str__(self):
        return self.nama
    
    
    
    def get_role(self):
        if hasattr(self, 'mahasiswa'):
            return self.mahasiswa.role
        elif hasattr(self, 'admin'):
            return self.admin.role
        return None 
    
class Mahasiswa(models.Model):
    role = models.CharField(max_length=255, default=Role.MHS.value)
    totalFollowers = models.IntegerField(default=0)
    totalUpvote = models.IntegerField(default=0)
    totalDownvote = models.IntegerField(default=0)
    pengguna = models.OneToOneField(Pengguna, on_delete=models.CASCADE, primary_key=True, default=None)
class Admin(models.Model):
    role = models.CharField(max_length=255, default=Role.ADM.value)
    pengguna = models.OneToOneField(Pengguna, on_delete=models.CASCADE, primary_key=True, default=None)

class BlacklistedToken(models.Model):
    token = models.CharField(max_length=255, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"BlacklistedToken: {self.token}"