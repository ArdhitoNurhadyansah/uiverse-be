from profiles.serializers import ProfileSerializer
from rest_framework import serializers
from .models import Pengguna, Mahasiswa, Admin

        
class PenggunaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pengguna
        fields = ['id', 'npm', 'nama', 'email']

class MahasiswaSerializer(serializers.ModelSerializer):
    # Use PenggunaSerializer to inherit the fields
    pengguna = PenggunaSerializer()
    profile = ProfileSerializer()
    class Meta:
        model = Mahasiswa
        fields = ['role', 'pengguna', 'totalFollowers', 'totalUpvote', 'totalDownvote', 'profile']


class AdminSerializer(serializers.ModelSerializer):
    # Use PenggunaSerializer to inherit the fields
    pengguna = PenggunaSerializer()

    class Meta:
        model = Admin
        fields = ['role', 'pengguna']
