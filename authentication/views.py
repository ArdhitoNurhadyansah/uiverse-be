
from django.http import JsonResponse, HttpRequest, HttpResponse, HttpResponseRedirect
from rest_framework_simplejwt.tokens import RefreshToken
from .models import Pengguna, Admin, Mahasiswa, Role, BlacklistedToken
from django.http import JsonResponse
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.views import APIView
from .serializers import MahasiswaSerializer, AdminSerializer
from .utils import BlacklistTokenAuthentication, authenticate
from django.utils import timezone

from .utils import getRoledUser, alreadyLoggedIn

class LoginMahasiswa(APIView):
    permission_classes = [AllowAny]
    def post(self, request, *args, **kwargs):
        print(request.user)
        data = request.data  
        try:
            username = data.get('username')
            password = data.get('password')
            # handle if mahassiwa already logged in
            
            resp = alreadyLoggedIn(request, username)
            
            if resp is not None:
                resp['role'] = Role.MHS.value
                return JsonResponse(
                    resp,
                    status=200
                ) 
            
            # handle logging in mahasiswa
            mahasiswa = authenticate(username, password, True)
            print("mahasiswa", mahasiswa)
            if mahasiswa is not None:
                # generate token
                refresh = RefreshToken.for_user((Pengguna.objects.get(id=mahasiswa.pengguna.id)))
                access_token = str(refresh.access_token)
                serializer = MahasiswaSerializer(mahasiswa)
                mahasiswa_serialized = serializer.data
                print(f'Token for {username}: {access_token}')
                resp = {
                        'message': 'SSO API request successful',
                        'token' : str(access_token),
                        'role': Role.MHS.value
                        }
                resp.update(mahasiswa_serialized)
                return JsonResponse(
                    resp,
                    status=200
                ) 
            else:
                return JsonResponse(
                    
                    {'error': 'UIVerse could Not authorize this user'}, 
                    status=401
                    
                )
        except Exception as e:
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            ) 
    
class LoginAdmin(APIView):
    permission_classes = [AllowAny]
    def post(self, request, *args, **kwargs):
        print(request.user)
        data = request.data  
        try:
            username = data.get('username')
            password = data.get('password')
            # handle if admin already logged in
            resp = alreadyLoggedIn(request, username)
            if resp is not None:
                resp['role'] = Role.ADM.value
                return JsonResponse(
                    resp,
                    status=200
                ) 
            # handle logging in admin
            admin = authenticate(username, password, False)
            print("admin " + str(admin))
            if admin is not None:
                # generate token
                print(admin.pengguna)
                refresh = RefreshToken.for_user((Pengguna.objects.get(id=admin.pengguna.id)))
                access_token = str(refresh.access_token)
                serializer = AdminSerializer(admin)
                admin_serialized = serializer.data

                print(f'Token for {username}: {access_token}')
                resp = {
                        'message': 'SSO API request successful',
                        'token' : str(access_token),
                        'role': Role.ADM.value
                        }
                resp.update(admin_serialized)
                return JsonResponse(
                    resp,
                    status=200
                ) 
            else:
                return JsonResponse(
                    
                    {'error': 'UIVerse could Not authorize this user'}, 
                    status=401
                    
                )
        except Exception as e:
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            ) 
class Logout(APIView):
    aauthentication_classes = [JWTAuthentication, BlacklistTokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        token = request.auth
        print(token)
        black_token = BlacklistedToken()
        black_token.token = token
        black_token.created_at = timezone.now()
        black_token.save()
        print(request.user)
        return JsonResponse(
                {'data': 'Succesfully logged out.'}, 
                status=200
            ) 