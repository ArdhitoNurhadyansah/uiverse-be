from rest_framework_simplejwt.tokens import AccessToken, RefreshToken

class CustomAccessToken(AccessToken):

    @classmethod
    def for_user(cls, user):
        token = super().for_user(user)

        user_role = user.get_role()
        if user_role:
            token['role'] = user_role

        return token
    
class CustomRefreshToken(RefreshToken):
    access_token_class = CustomAccessToken