from rest_framework import permissions
from .models import Admin, Mahasiswa, Role

class IsAdmin(permissions.BasePermission):
    message = 'You must be an admin to perform this action.'

    def has_permission(self, request, view):
        # token = request.headers.get('Authorization')  
        # return token.role == Role.ADM.value
        return request.headers.get('role') == Role.ADM.value

class IsMahasiswa(permissions.BasePermission):
    message = 'You must be a mahasiswa to perform this action.'

    def has_permission(self, request, view):
        return request.headers.get('role') == Role.MHS.value

        ## 2nd option
        # token = request.headers.get('Authorization')  
        # return token.role == Role.ADM.value