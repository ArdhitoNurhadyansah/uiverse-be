# backends.py
import json

from .serializers import AdminSerializer, MahasiswaSerializer
from django.conf import settings
from profiles.models import Profile
import requests
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.db.models import Q
from .models import Pengguna, Mahasiswa, Admin, Role
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
from .models import BlacklistedToken


LANG = settings.SSO_UI_ORG_DETAIL_LANG
ORG_CODE = {}
with open(settings.SSO_UI_ORG_DETAIL_FILE_PATH, 'r') as ORG_CODE_FILE:
    ORG_CODE.update(json.load(ORG_CODE_FILE))

class BlacklistTokenAuthentication(BaseAuthentication):
    def authenticate(self, request):
        # Get the token from the request header
        token = request.headers.get('Authorization')

        if token:
            # Extract the token value from the Authorization header
            # For example, if the header is 'Bearer <token>', extract '<token>'
            token = token.split(' ')[1] if 'Bearer' in token else token

            # Check if the token is blacklisted
            if BlacklistedToken.objects.filter(token=token).exists():
                raise AuthenticationFailed('Token is blacklisted')

            # Add your logic to authenticate the user based on the token
            # ...

        return None



def authenticate(username=None, password=None, isMahasiswa=True):

        api_url = "https://api.cs.ui.ac.id/authentication/ldap/v2/"
        data = {
            "username" : username,
            "password" : password
        } 
        try:
            print("sebelum request")
            response = requests.post(api_url, data=data)
            print(response.json().get('state'))
            if (response.status_code == 200 and response.json().get('state') == 1) or (response.json().get('state') == 0 and username == 'rektor.ceritanya')  \
                or (response.json().get('state') == 0 and username == 'mahasiswa.ceritanya') \
                or (response.json().get('state') == 0 and username == 'mahasiswa.lain'):
                data = response.json()
                try:
                    print("masuk try")
                    pengguna_loggedin = Pengguna.objects.filter(id=username).first()
                    print("pengguna_loggedin", pengguna_loggedin)
                    if pengguna_loggedin is None:
                        pengguna = Pengguna()
                        pengguna.id = data.get('username')
                        pengguna.npm = data.get('kodeidentitas')
                        pengguna.nama = data.get('nama')
                        pengguna.kd_org = data.get('kode_org')
                        pengguna.email = f"{data.get('username')}@ui.ac.id"
                        pengguna.username = data.get('username')
                        pengguna.save()
                        # cek kalo mahasiswa
                        if isMahasiswa:
                            mahasiswa = Mahasiswa()
                            mahasiswa.pengguna = pengguna
                            mahasiswa.save()
                            kode_organisasi = ORG_CODE[LANG][data.get('kode_org').split(':')[0]]
                            print("masuk signal", kode_organisasi)
                            new_profile = Profile.objects.create(
                                fakultas=kode_organisasi['faculty'],
                                jurusan=kode_organisasi['study_program'],
                                program=kode_organisasi['educational_program'],
                                angkatan=f'20{data.get("kodeidentitas")[:2]}',
                                mahasiswa=mahasiswa
                            )

                            return mahasiswa
                        else: 
                            admin = Admin()
                            admin.pengguna = pengguna
                            admin.save()
                            return admin
                    else:
                        print("loggedIn", pengguna_loggedin)
                        # cek kalo mahasiswa
                        print("sini")
                        if isMahasiswa:
                            
                            if pengguna_loggedin.mahasiswa is None:
                                mahasiswa = Mahasiswa()
                                mahasiswa.pengguna = pengguna_loggedin
                                mahasiswa.save()
                                return mahasiswa
                            else:
                                return pengguna_loggedin.mahasiswa
                        else: 
                            print("hey")
                            if getattr(pengguna_loggedin, 'admin', None) is None:
                                print("admin is None")
                                admin = Admin()
                                admin.pengguna = pengguna_loggedin
                                admin.save()
                                return admin
                            else:
                                print("admin is not None")
                                return pengguna_loggedin.admin
                except Exception as e:
                    print("except", str(e))
                    return None
            else:
                print("account not found")
                return None
        except Exception as e:
            print("error sini", str(e))
            return None

# get serialized data based on role else return None
def getRoledUser(pengguna, role):
    try:
        if role == Role.MHS.value:
            mahasiswa = pengguna.mahasiswa
            serializer = MahasiswaSerializer(mahasiswa)
            mahasiswa_serialized = serializer.data
            return mahasiswa_serialized
        elif role == Role.ADM.value:
            admin = pengguna.admin
            serializer = AdminSerializer(admin)
            admin_serialized = serializer.data
            return admin_serialized
        else: return None
    except Exception as e:
        print(str(e))
        return None
    
# handle return dict for json response if user already logged in and none if not or error
def alreadyLoggedIn(request, username):    
    if not request.user.is_authenticated:
        return None
    
    
    token = request.headers.get('Authorization')
    
    if token:
        token = token.split(' ')[1] if 'Bearer' in token else token
        
        role = request.headers.get('role')
        if role is None or role == '':
            print('role not found')
    
            return None
        
        
    # if 'auth_token' in request.session:
    #     if 'role' not in request.session:
    #         print('role not found')
    #         return None
        pengguna = Pengguna.objects.filter(id=username).first()
        if pengguna is None:
            print('user not found')
            return None
        # role = request.session.get('role')
        resp = {
                'message': 'User already logged in',
                'token' : token,
                }
        roledUser = getRoledUser(pengguna, role)
        resp.update(roledUser)
        return resp
    else: return None