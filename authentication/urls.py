from .views import LoginMahasiswa, Logout, LoginAdmin
from django.urls import path

urlpatterns = [
    path('login/mahasiswa/', LoginMahasiswa.as_view(), name='login-mhs'),
    path('login/admin/', LoginAdmin.as_view(), name='login-adm'),
    path('logout/', Logout.as_view(), name='logout')
]