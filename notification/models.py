from django.db import models
from authentication.models import Pengguna, Mahasiswa
import uuid

# Create your models here.
class NotificationFormat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    message = models.TextField()

class EventType(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    eventName = models.TextField()

class Notification(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    notification_format = models.ForeignKey(NotificationFormat,on_delete=models.CASCADE)
    finalMessage = models.TextField()
    reciever = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    sender = models.ForeignKey(Pengguna, on_delete=models.CASCADE)
    event = models.ForeignKey(EventType, on_delete=models.CASCADE)