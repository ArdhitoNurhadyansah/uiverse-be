from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from authentication.permissions import IsMahasiswa
from django.http import JsonResponse
from authentication.models import Pengguna


from .models import Notification, NotificationFormat, EventType
from .serializers import NotificationSerializer

# Create your views here.
# List of events:
events = ['UPVOTE', 'DOWNVOTE', 'REPLY', 'ANSWER', 'FOLLOW']

@permission_classes([IsAuthenticated, IsMahasiswa])
@api_view(['POST'])
def create_notification(request):
    try:
        sender = request.data.get('sender')
        reciever = request.data.get('reciever')
        finalMessage = request.data.get('finalMessage')
        new_notification = Notification.objects.create(finalMessage, reciever, sender)
        serializer = NotificationSerializer(new_notification)
        response = {
            'message': 'Notification Pushed!',
            'notification': serializer.data
        }
        return JsonResponse(response, status=200)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

@permission_classes([IsAuthenticated, IsMahasiswa])
@api_view(['GET'])
def get_all_notification_by_user_id(request, mahasiswa_id):
    try:
        pengguna = Pengguna.objects.get(id=mahasiswa_id)
        mahasiswa = pengguna.mahasiswa
        notifications = Notification.objects.filter(reciever=mahasiswa)
        serializer = NotificationSerializer(notifications, many=True)
        return JsonResponse(serializer.data, status=200)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
