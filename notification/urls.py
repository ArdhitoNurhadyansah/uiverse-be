from .views import *
from django.urls import path

urlpatterns = [
    path("create/", create_notification, name="create"),
    path("notify/", get_all_notification_by_user_id, name="notify")
]
