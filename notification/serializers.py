from authentication.serializers import PenggunaSerializer, MahasiswaSerializer
from rest_framework import serializers
from .models import Notification, NotificationFormat, EventType
from authentication.serializers import PenggunaSerializer, MahasiswaSerializer

class NotificationFormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationFormat
        fields = '__all__'

class EventTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventType
        fields = '__all__'

class NotificationSerializer(serializers.ModelSerializer):
    notification_format = NotificationFormatSerializer()
    reciever = MahasiswaSerializer()
    sender = PenggunaSerializer()
    event = EventTypeSerializer()

    class Meta:
        model = Notification
        fields = '__all__'
