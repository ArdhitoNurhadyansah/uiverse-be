1. Buat environment baru dengan nama `env` dengan perintah `python3 -m venv env`
2. Aktifkan environment dengan perintah `source env/bin/activate` untuk MacOS dan Linux, atau `\env\Scripts\activate` untuk Windows
3. Install semua dependencies dengan perintah `pip install -r requirements.txt`
3. Download file prod-ca-uiverse.crt.
4. Buat file `.env` dengan isi sebagai berikut:
```env
SUPABASE_DB_HOST=
SUPABASE_DB_NAME=
SUPABASE_DB_USER=
SUPABASE_DB_PASSWORD=
SUPABASE_DB_PORT=
SUPABASE_SSL_CERTIFICATE= <path/menuju/prod-ca-uiverse.crt>
```
5. Migrasi database dengan perintah `python manage.py migrate`
6. Jalankan server dengan perintah `python manage.py runserver`