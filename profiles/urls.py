from django.urls import path
from .views import get_current_pengguna, get_other_pengguna, update_profil_pengguna

urlpatterns = [
    path('', get_current_pengguna, name='get_current_pengguna'),
    path('update/', update_profil_pengguna, name='update_profile'),
    path('<str:id>/',  get_other_pengguna, name='get_other_pengguna')
]