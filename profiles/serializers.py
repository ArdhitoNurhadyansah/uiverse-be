from rest_framework import serializers
from .models import Profile

class ProfileSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Profile
        fields = ['fakultas', 'jurusan', 'angkatan', 'program', 'tentang_saya', 'profile_picture']
        read_only_fields = ['fakultas', 'jurusan', 'program']