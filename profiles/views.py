from authentication.permissions import IsMahasiswa
from .serializers import ProfileSerializer
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from authentication.utils import getRoledUser
from rest_framework.decorators import permission_classes

from authentication.models import Mahasiswa, Pengguna, Role

# Create your views here.

@api_view(['GET'])
def get_current_pengguna(request):
    try:
        pengguna = request.user
        role = request.headers.get('role')
        roledUser = getRoledUser(pengguna, role)
        if roledUser is not None:
            return JsonResponse(
                roledUser,
                status=200
            ) 
        else:
            return JsonResponse(
            {'error': 'Unauthorized'}, 
            status=401
        ) 
    except Exception as e:
        return JsonResponse(
            {'error': str(e)}, 
            status=400
        ) 

@api_view(['GET'])
def get_other_pengguna(request, id):
    pengguna_lain = get_object_or_404(Pengguna, id=id)
    mahasiswa_lain = getRoledUser(pengguna_lain, Role.MHS.value)
    if mahasiswa_lain is not None:
        return JsonResponse(
            mahasiswa_lain,
            status=200
        ) 
    else:
        return JsonResponse(
            {'error': 'Unauthorized'}, 
            status=401
        )
    
@api_view(['PUT', 'PATCH'])
@permission_classes([IsMahasiswa])
def update_profil_pengguna(request):
    pengguna = request.user
    mahasiswa = Mahasiswa.objects.filter(pengguna=pengguna).first()
    if mahasiswa is None:
        return JsonResponse(
            {'error': 'Unauthorized'}, 
            status=401
        )
    data = request.data
    serializer = ProfileSerializer(mahasiswa.profile, data=data, partial=True)
    
    if serializer.is_valid():
        serializer.save()
        return JsonResponse(
            serializer.data,
            status=200
        )
    else:
        return JsonResponse(
            serializer.errors,
            status=400
        )
    