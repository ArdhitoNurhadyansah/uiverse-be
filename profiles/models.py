from django.db import models
from authentication.models import Mahasiswa

# Create your models here.
class Profile(models.Model):
    mahasiswa = models.OneToOneField(Mahasiswa, on_delete=models.CASCADE, null=True, blank=True)
    fakultas = models.CharField(max_length=255)
    jurusan = models.CharField(max_length=255)
    angkatan = models.CharField(max_length=255)
    program = models.CharField(max_length=255)
    tentang_saya = models.TextField()
    profile_picture = models.ImageField(upload_to='static/profile_pictures/', null=True, blank=True)

