from django.db import models
from authentication.models import Mahasiswa
from django.utils import timezone
import uuid
import pytz

# Model Manager untuk memfilter tanggapan postingan yang merupakan question dan bukan question
class AnswerManager(models.Manager):
    def get_queryset(self):
        # Filter semua postingan yang merupakan question
        question = Question.objects.all()
        return super().get_queryset().filter(postingan__in=question)

class ReplyManager(models.Manager):
    def get_queryset(self):
        # Filter semua postingan yang bukan merupakan question
        question = Question.objects.all()
        return super().get_queryset().exclude(postingan__in=question)
    
class Postingan(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    content = models.TextField()
    totalUpvote = models.IntegerField(default=0)
    totalDownvote = models.IntegerField(default=0)
    createdAt = models.DateTimeField(auto_now_add=True)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    kategori = models.ForeignKey('KategoriPostingan', on_delete=models.CASCADE, default=1)
    is_taken_down = models.BooleanField(default=False)

class KategoriPostingan(models.Model):
    name = models.CharField(max_length=255)

class Tanggapan(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4, editable=False)
    postingan = models.ForeignKey(Postingan, on_delete=models.CASCADE)
    mahasiswa = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    is_taken_down = models.BooleanField(default=False)

    objects = models.Manager()
    answer = AnswerManager()
    reply = ReplyManager()

class Question(Postingan):
    totalFollowers = models.IntegerField(default=0)
    followers = models.ManyToManyField(Mahasiswa, related_name='question_followers')
    
class Vote(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    voter = models.ForeignKey(Mahasiswa, on_delete=models.CASCADE)
    postingan = models.ForeignKey(Postingan, on_delete=models.CASCADE, null=True, blank=True)
    tanggapan = models.ForeignKey(Tanggapan, on_delete=models.CASCADE, null=True, blank=True)
    is_upvote = models.BooleanField(default=True)
    is_downvote = models.BooleanField(default=False)