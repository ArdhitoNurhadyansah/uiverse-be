# serializers.py
from rest_framework import serializers
from .models import KategoriPostingan, Postingan, Tanggapan, Question, Vote
from authentication.serializers import MahasiswaSerializer
import pytz

class KategoriPostinganSerializer(serializers.ModelSerializer):

    class Meta:
        model = KategoriPostingan
        fields = '__all__'

class PostinganSerializer(serializers.ModelSerializer):
    # Serialize the mahasiswa attribute directly
    mahasiswa = serializers.SerializerMethodField()
    kategori = KategoriPostinganSerializer()

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        # Set timezone to WIB (Asia/Jakarta)
        wib_timezone = pytz.timezone('Asia/Jakarta')
        instance.createdAt = instance.createdAt.astimezone(wib_timezone)

        # Format createdAt field
        representation['createdAt'] = instance.createdAt.strftime("%Y-%m-%d %H:%M:%S")
        return representation

    class Meta:
        model = Postingan
        fields = '__all__'

    def get_mahasiswa(self, instance):
        # Use MahasiswaSerializer to serialize the mahasiswa attribute
        mahasiswa_serializer = MahasiswaSerializer(instance.mahasiswa)
        return mahasiswa_serializer.data
    
class TanggapanSerializer(serializers.ModelSerializer):

    mahasiswa = serializers.SerializerMethodField()
    postingan = PostinganSerializer()

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        wib_timezone = pytz.timezone('Asia/Jakarta')
        instance.timestamp = instance.timestamp.astimezone(wib_timezone)

        representation['timestamp'] = instance.timestamp.strftime("%Y-%m-%d %H:%M:%S")
        return representation

    class Meta:
        model = Tanggapan
        fields = '__all__'

    def get_mahasiswa(self, instance):
        mahasiswa_serializer = MahasiswaSerializer(instance.mahasiswa)
        return mahasiswa_serializer.data
    
class QuestionSerializer(serializers.ModelSerializer):
    mahasiswa = serializers.SerializerMethodField()
    kategori = KategoriPostinganSerializer()

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        # Set timezone to WIB (Asia/Jakarta)
        wib_timezone = pytz.timezone('Asia/Jakarta')
        instance.createdAt = instance.createdAt.astimezone(wib_timezone)

        # Format createdAt field
        representation['createdAt'] = instance.createdAt.strftime("%Y-%m-%d %H:%M:%S")
        return representation

    class Meta:
        model = Question
        fields = '__all__'

    def get_mahasiswa(self, instance):
        # Use MahasiswaSerializer to serialize the mahasiswa attribute
        mahasiswa_serializer = MahasiswaSerializer(instance.mahasiswa)
        return mahasiswa_serializer.data




class VoteSerializer(serializers.ModelSerializer):
    voter = MahasiswaSerializer()
    postingan = PostinganSerializer()

    class Meta:
            model = Vote
            fields = ['id', 'voter', 'postingan', 'is_upvote', 'is_downvote']
