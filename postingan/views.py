from django.shortcuts import get_object_or_404, render
from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from .models import KategoriPostingan, Postingan, Tanggapan, Question, Vote
from .serializers import PostinganSerializer, TanggapanSerializer, QuestionSerializer, VoteSerializer
from authentication.models import Admin, Pengguna
from authentication.permissions import IsMahasiswa
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from authentication.utils import BlacklistTokenAuthentication
from authentication.models import Role
from rest_framework import viewsets

# Create your views here.

# Postingan

@api_view(['POST'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def create_postingan(request):
    try: 
        pengguna = request.user 
        mahasiswa = pengguna.mahasiswa
        content = request.data.get('content')
        kategori_id = request.data.get('kategori')
        kategori = KategoriPostingan.objects.get(id=kategori_id) 
        
        new_postingan = Postingan.objects.create(content=content, mahasiswa=mahasiswa, kategori=kategori)
        serializer = PostinganSerializer(new_postingan)
        resp = {
            'message': 'Postingan successfully created!',
            'postingan': serializer.data,  
        }
        return JsonResponse(resp, status=200)
    except Exception as e:
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            ) 
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_postingan(request, postingan_id):
    try:
        postingan = Postingan.objects.get(id=postingan_id)
        serializer = PostinganSerializer(postingan)
        
        # Mengambil Vote
        pengguna = request.user 
        mahasiswa = pengguna.mahasiswa
        vote = Vote.objects.filter(postingan=postingan, voter=mahasiswa).first()
        is_mine = postingan.mahasiswa.pengguna.id == pengguna.id
        is_upvote = False
        is_downvote = False
        
        print(is_mine)
        if vote is not None:
            print(vote.is_upvote)
            print(vote.is_downvote)
            is_upvote = vote.is_upvote
            is_downvote = vote.is_downvote
        else:
            print("belum vote")

        # Mengambil reply
        replies = Tanggapan.reply.filter(postingan=postingan)
        reply_serializer = TanggapanSerializer(replies, many=True) 
        print(replies)
        return JsonResponse({
            'yell': serializer.data,
            'reply':reply_serializer.data,
            'total_reply':len(replies),
            'is_upvote': is_upvote,
            'is_downvote': is_downvote,
            'is_mine': is_mine,
        }, status=200)
    except Postingan.DoesNotExist:
        return JsonResponse({'error': 'Postingan not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_all_postingans(request):
    try:
        postingans = Postingan.objects.all()
        serializer = PostinganSerializer(postingans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_postingans_by_mahasiswa(request, mahasiswa_id):
    try:
        pengguna = Pengguna.objects.get(id=mahasiswa_id)
        mahasiswa = pengguna.mahasiswa
        if mahasiswa:
            postingans = Postingan.objects.filter(mahasiswa=mahasiswa)
            serializer = PostinganSerializer(postingans, many=True)
            return JsonResponse(serializer.data, status=200, safe=False)
        else:
            return JsonResponse({'error': 'Mahasiswa not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def delete_postingan(request, postingan_id):
    try:
        postingan = Postingan.objects.get(id=postingan_id)

        # Ensure the user making the request owns the postingan
        if postingan.mahasiswa.pengguna != request.user:
            return JsonResponse({'error': 'Permission denied'}, status=403)
        postingan.delete()
        return JsonResponse({'message': 'Postingan successfully deleted'}, status=200)
    except Postingan.DoesNotExist:
        return JsonResponse({'error': 'Postingan not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

@api_view(['PUT', 'PATCH'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def update_postingan(request, postingan_id):
    try:
        postingan = Postingan.objects.get(id=postingan_id)

        print(postingan_id)
        # Ensure the user making the request owns the postingan
        if postingan.mahasiswa.pengguna != request.user:
            return JsonResponse({'error': 'Permission denied'}, status=403)
        
        # Update the content field with the new value
        new_content = request.data.get('content')
        kategori_id = request.data.get('kategori')
        new_kategori = KategoriPostingan.objects.get(id=kategori_id) 
        if request.method == 'PUT':
                postingan.content = new_content
                postingan.kategori = new_kategori
        elif request.method == 'PATCH':
            if new_content:
                postingan.content = new_content
            if new_kategori:
                postingan.kategori = new_kategori
        postingan.save()
        serializer = PostinganSerializer(postingan)
        return JsonResponse({'message': 'Postingan successfully updated', 'postingan': serializer.data}, status=200)
    except Postingan.DoesNotExist:
        return JsonResponse({'error': 'Postingan not found'}, status=404)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_postingans_by_kategori(request, kategori):
    try:
        # Filter postingan berdasarkan kategori
        postingans = Postingan.objects.filter(kategori=kategori)
        serializer = PostinganSerializer(postingans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
# Vote

@api_view(['POST'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def vote(request, postingan_id):
    try:
        pengguna = request.user 
        mahasiswa = pengguna.mahasiswa
        vote = request.data.get('vote')

        # Validate vote_type
        if vote not in ['upvote', 'downvote']:
            return JsonResponse({'error': 'Invalid vote type'}, status=400)
        
        postingan = Postingan.objects.get(id=postingan_id)
        contributor = postingan.mahasiswa
        existing_vote = Vote.objects.filter(voter=mahasiswa, postingan=postingan).first()

        # If the vote already exists, update the vote type
        if existing_vote:

            # Handle Before
            if existing_vote.is_upvote:
                contributor.totalUpvote -= 1
                postingan.totalUpvote -= 1
                existing_vote.is_upvote = False
            elif existing_vote.is_downvote:
                contributor.totalDownvote -= 1
                postingan.totalDownvote -= 1
                existing_vote.is_downvote = False

            # Handle After
            if vote == 'upvote':
                contributor.totalUpvote += 1
                postingan.totalUpvote += 1
                existing_vote.is_upvote = True
            elif vote == 'downvote':
                contributor.totalDownvote += 1
                postingan.totalDownvote += 1
                existing_vote.is_downvote = True
            contributor.save()
            postingan.save()
            existing_vote.save()

            serializer = VoteSerializer(existing_vote)
            resp = {
                'message': 'Vote successfully updated',
                'vote': serializer.data,  
            }
            return JsonResponse(resp, status=200)
        
        # If the vote doesn't exist, create a new vote
        else:
            
            new_vote = Vote()
            new_vote.voter = mahasiswa
            new_vote.postingan = postingan
            if vote == 'upvote':
                contributor.totalUpvote += 1
                postingan.totalUpvote += 1
                new_vote.is_upvote = True
                new_vote.is_downvote = False
            elif vote == 'downvote':
                contributor.totalDownvote += 1
                postingan.totalDownvote += 1
                new_vote.is_upvote = False
                new_vote.is_downvote = True
            contributor.save()
            postingan.save()
            new_vote.save()

            serializer = VoteSerializer(new_vote)
            resp = {
                'message': 'Vote successfully created',
                'vote': serializer.data,  
            }
            return JsonResponse(resp, status=200)
        
    except Exception as e:
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            )

# Yell
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_all_yell(request):
    try:
        # Mengambil semua postingan yang bukan pertanyaan dan mengurutkannya berdasarkan createdAt secara ascending
        postingans = Postingan.objects.exclude(question__isnull=False).order_by('createdAt')
        serializer = PostinganSerializer(postingans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_yell_by_mahasiswa(request, mahasiswa_id):
    try:
        # Mengambil mahasiswa berdasarkan ID
        pengguna = Pengguna.objects.get(id=mahasiswa_id)
        mahasiswa = pengguna.mahasiswa

        # Mengambil semua postingan yang bukan pertanyaan milik mahasiswa tersebut
        postingans = Postingan.objects.filter(mahasiswa=mahasiswa).exclude(question__isnull=False)

        serializer = PostinganSerializer(postingans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def get_yell_by_kategori(request, kategori):
    try:
        # Mengambil semua postingan yang bukan pertanyaan dan sesuai dengan kategori
        postingans = Postingan.objects.filter(kategori=kategori).exclude(question__isnull=False)

        serializer = PostinganSerializer(postingans, many=True)
        return JsonResponse(serializer.data, status=200, safe=False)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
    
# Tanggapan
@api_view(['POST'])
@permission_classes([IsAuthenticated, IsMahasiswa])
def create_tanggapan(request):
    pengguna = request.user
    mahasiswa = pengguna.mahasiswa

    content = request.data.get('content')
    tanggapan_object = Tanggapan.objects.create(content=content, mahasiswa=mahasiswa).first()
    serializer = TanggapanSerializer(tanggapan_object)
    response = {
        'message': 'Tanggapan is good to go!',
        'tanggapan': serializer.data
    }
    return JsonResponse(response, status=200)

# Question

@permission_classes([IsMahasiswa])
class QuestionAPI(viewsets.ModelViewSet):
    serializer_class = QuestionSerializer  
    authentication_classes = [JWTAuthentication, BlacklistTokenAuthentication]
    def post(self, request):
        try: 
            pengguna = request.user 
            mahasiswa = pengguna.mahasiswa
            content = request.data.get('content')
            kategori_id = request.data.get('kategori')

            kategori = KategoriPostingan.objects.get(id=kategori_id)
            print(content)
            print(kategori)
            new_question = Question()
            new_question.content = content
            new_question.mahasiswa = mahasiswa
            new_question.kategori = kategori
            new_question.save()
            serializer = QuestionSerializer(new_question)
            resp = {
                'message': 'Question successfully created!',
                'question': serializer.data,  
            }
            return JsonResponse(resp, status=200)
        except Exception as e:
                print(str(e))
                return JsonResponse(
                    {'error': str(e)}, 
                    status=400
                ) 
    def follow(self, request, question_id):
        try: 
            pengguna = request.user 
            mahasiswa = pengguna.mahasiswa
            question = Question.objects.get(id=question_id)
            # print(question.followers.filter(id=pengguna.id))
            if mahasiswa not in question.followers.all():
                question.followers.add(mahasiswa)
                question.totalFollowers = question.followers.count()
                question.save()

                serializer = QuestionSerializer(question)
                return JsonResponse({'message': 'Follower added successfully', 'question': serializer.data}, status=200)
            else:
                serializer = QuestionSerializer(question)
                return JsonResponse({'message': 'You already followed this question', 'question': serializer.data}, status=200)

        except Question.DoesNotExist:
            return JsonResponse({'error': 'Question not found'}, status=404)
        except Exception as e:
                print(str(e))
                return JsonResponse(
                    {'error': str(e)}, 
                    status=400
                ) 

    def get_detail(self, request, question_id):
        try:
            question = Question.objects.get(id=question_id)
            serializer = QuestionSerializer(question)
            
            # Mengambil Vote
            pengguna = request.user 
            mahasiswa = pengguna.mahasiswa
            vote = Vote.objects.filter(postingan=question, voter=mahasiswa).first()
            is_mine = question.mahasiswa.pengguna.id == pengguna.id
            is_upvote = False
            is_downvote = False
            is_followed = False
            # Cek is Followed:
            if mahasiswa in question.followers.all():
                is_followed = True
            # if question.followers and mahasiswa in question.followers:
            #     is_followed = True
            print("udah follow blm")
            print(question.followers)
            print(is_mine)
            if vote is not None:
                print(vote.is_upvote)
                print(vote.is_downvote)
                is_upvote = vote.is_upvote
                is_downvote = vote.is_downvote
            else:
                print("belum vote")

            return JsonResponse({
                'question': serializer.data,
                'is_upvote': is_upvote,
                'is_downvote': is_downvote,
                'is_mine': is_mine,
                'is_followed' : is_followed
            }, status=200)
        except Question.DoesNotExist:
            return JsonResponse({'error': 'Question not found'}, status=404)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

    def get_by_mahasiswa(self, request, mahasiswa_id):
        try:
            pengguna = Pengguna.objects.get(id=mahasiswa_id)
            mahasiswa = pengguna.mahasiswa
            if mahasiswa:
                questions = Question.objects.filter(mahasiswa=mahasiswa)
                serializer = QuestionSerializer(questions, many=True)
                return JsonResponse(serializer.data, status=200, safe=False)
            else:
                return JsonResponse({'error': 'Mahasiswa not found'}, status=404)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

    def get_by_kategori(self, request, kategori):
        try:
            # Filter postingan berdasarkan kategori
            questions = Question.objects.filter(kategori=kategori)
            serializer = QuestionSerializer(questions, many=True)
            return JsonResponse(serializer.data, status=200, safe=False)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

    def get_all(self, request):
        try:
            questions = Question.objects.all()
            serializer = QuestionSerializer(questions, many=True)
            return JsonResponse(serializer.data, status=200, safe=False)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)
        
@api_view(['GET'])
@permission_classes([IsMahasiswa])
def get_reply_by_yell(request, yell_id):
    replies = Tanggapan.reply.filter(postingan=yell_id)
    serializer = TanggapanSerializer(replies, many=True) 

    return JsonResponse(serializer.data, status=200, safe=False)

@api_view(['POST'])
@permission_classes([IsMahasiswa])
def create_reply(request, yell_id):
    try: 
        pengguna = request.user 
        mahasiswa = pengguna.mahasiswa
        content = request.data.get('content')
        yell = Postingan.objects.get(id=yell_id)
        tanggapan_object = Tanggapan.objects.create(content=content, mahasiswa=mahasiswa, postingan=yell)
        serializer = TanggapanSerializer(tanggapan_object)
        response = {
            'message': 'Reply is good to go!',
            'tanggapan': serializer.data
        }
        return JsonResponse(response, status=200)
    except Exception as e:
            print(str(e))
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            )

@api_view(['DELETE'])
def delete_reply(request, reply_id):
    pengguna = request.user
    print(pengguna)
    # cek pengguna adalah admin atau bukan
    admin = Admin.objects.filter(pengguna=pengguna).first()
    print(admin)
    reply = get_object_or_404(Tanggapan, id=reply_id)
    
    # Pastikan pengguna yang menghapus reply adalah pemilik reply, tapi perbolehkan jika pengguna adalah admin
    if reply.mahasiswa.pengguna != pengguna and admin is None:
        return JsonResponse({'error': 'Permission denied'}, status=403)
    
    reply.delete()
    return JsonResponse({'message': 'Reply successfully deleted'}, status=204)

# Answer
@api_view(['GET'])
@permission_classes([IsMahasiswa])
def get_answer_by_question(request, question_id):
    answers = Tanggapan.answer.filter(postingan=question_id)
    serializer = TanggapanSerializer(answers, many=True) 

    return JsonResponse(serializer.data, status=200, safe=False)

@api_view(['POST'])
@permission_classes([IsMahasiswa])
def create_answer(request, question_id):
    try: 
        pengguna = request.user 
        mahasiswa = pengguna.mahasiswa
        content = request.data.get('content')
        question = Postingan.objects.get(id=question_id)
        tanggapan_object = Tanggapan.objects.create(content=content, mahasiswa=mahasiswa, postingan=question)
        serializer = TanggapanSerializer(tanggapan_object)
        response = {
            'message': 'Answer is good to go!',
            'tanggapan': serializer.data
        }
        return JsonResponse(response, status=200)
    except Exception as e:
            print(str(e))
            return JsonResponse(
                {'error': str(e)}, 
                status=400
            )
    
@api_view(['GET'])
@permission_classes([IsMahasiswa])
def get_detail_answer(request, answer_id):
    answer = get_object_or_404(Tanggapan, id=answer_id)
    serializer = TanggapanSerializer(answer)

    return JsonResponse(serializer.data, status=200)

@api_view(['PUT', 'PATCH'])
@permission_classes([IsMahasiswa])
def edit_answer(request, answer_id):
    try:
        pengguna = request.user
        mahasiswa = pengguna.mahasiswa
        answer = get_object_or_404(Tanggapan, id=answer_id)

        if answer.mahasiswa != mahasiswa:
            return JsonResponse({'error': 'Permission denied'}, status=403)
         
        new_content = request.data.get('content')

        if new_content:
            answer.content = new_content
            answer.save()

        serializer = TanggapanSerializer(answer)
        return JsonResponse({'message': 'Answer successfully updated', 'answer': serializer.data}, status=200)

    except Exception as e:
        print(str(e))
        return JsonResponse(
            {'error': str(e)}, 
            status=400
        )
    
@api_view(['DELETE'])
def delete_answer(request, answer_id):
    pengguna = request.user
    print(pengguna)
    # cek pengguna adalah admin atau bukan
    admin = Admin.objects.filter(pengguna=pengguna).first()
    print(admin)
    answer = get_object_or_404(Tanggapan, id=answer_id)
    
    # Pastikan pengguna yang menghapus answer adalah pemilik answer, tapi perbolehkan jika pengguna adalah admin
    if answer.mahasiswa.pengguna != pengguna and admin is None:
        return JsonResponse({'error': 'Permission denied'}, status=403)
    
    answer.delete()
    return JsonResponse({'message': 'Answer successfully deleted'}, status=204)