from .views import *
from django.urls import path
from .views import QuestionAPI

urlpatterns = [
    # Postingan
    path('', get_all_postingans, name='get-postingans'),
    path('<uuid:postingan_id>/', get_postingan, name='get-postingan'),
    path('create/', create_postingan, name='create-postingan'),
    path('mahasiswa/<str:mahasiswa_id>/', get_postingans_by_mahasiswa, name='get-postingans-mhs'),
    path('delete/<uuid:postingan_id>/', delete_postingan, name='delete-postingan'),
    path('update/<uuid:postingan_id>/', update_postingan, name='update-postingan'),
    path('kategori/<str:kategori>/', get_postingans_by_kategori, name='get-postingans-kat'),
    path('vote/<uuid:postingan_id>/', vote, name='vote'),

    # Yell
    path('yell/mahasiswa/<str:mahasiswa_id>/', get_yell_by_mahasiswa, name='get-yell-mhs'),
    path('yell/', get_all_yell, name='get-yells'),
    path('yell/kategori/<str:kategori>/', get_yell_by_kategori, name='get-yell-kat'),

    # Tanggapan
    path('create-tanggapan/', create_tanggapan, name='create-tanggapan'), # not used

    # Question
    path('question/', QuestionAPI.as_view({'get': 'get_all','post': 'post'}) , name='question-list'),
    path('question/<uuid:question_id>/', QuestionAPI.as_view({'get' : 'get_detail'}) , name='question-detail'),
    path('question/mahasiswa/<str:mahasiswa_id>/', QuestionAPI.as_view({'get' : 'get_by_mahasiswa'}) , name='question-by-mahasiswa'),
    path('question/kategori/<str:kategori>/', QuestionAPI.as_view({'get' : 'get_by_kategori'}) , name='question-by-kategori'),
    path('question/follow/<str:question_id>/', QuestionAPI.as_view({'post' : 'follow'}) , name='question-follow'),

    # Reply Yell
    path('reply/<uuid:yell_id>/', get_reply_by_yell, name='get-reply-by-yell'),
    path('reply/create/<uuid:yell_id>/', create_reply, name='create-reply'),
    path('reply/delete/<uuid:reply_id>/', delete_reply, name='delete-reply'),

    # Answer
    path('answer/all/<uuid:question_id>/', get_answer_by_question, name='get-answer-by-question'),
    path('answer/create/<uuid:question_id>/', create_answer, name='create-answer'),
    path('answer/detail/<uuid:answer_id>/', get_detail_answer, name='get-detail-answer'),
    path('answer/update/<uuid:answer_id>/', edit_answer, name='edit-answer'),
    path('answer/delete/<uuid:answer_id>/', delete_answer, name='delete-answer'),

]